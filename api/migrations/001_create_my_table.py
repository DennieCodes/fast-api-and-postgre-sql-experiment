steps = [
  [
      ## Create the table - this is what runs when you put python -m migrations up
      """
      CREATE TABLE vacations (
        id SERIAL PRIMARY KEY NOT NULL,
        name VARCHAR(1000) NOT NULL,
        from_date DATE NOT NULL,
        to_date DATE NOT NULL,
        thoughts TEXT
      );
      """,
      ## Drop the table - what runs when you put python -m migrations down
      """
      DROP TABLE vacations;
      """
  ]
]