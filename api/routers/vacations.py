from fastapi import APIRouter, Depends, Response
from typing import List, Union # allows for more than one thing
from queries.vacations import (
  Error,
  VacationIn,
  VacationRepository,
  VacationOut
)

router = APIRouter()

# CREATE A VACATION
@router.post("/vacations", response_model=Union[VacationOut, Error]) # VacationOut defines shape of our response in this case object of vacationout
def create_vacation(
  vacation: VacationIn, # vacation: VacationIn -> called type hints it will take JSON and turn it into an object for us to use via VacationIn
  response: Response,
  repo: VacationRepository = Depends()
):
  # repo, dependency injection where you say repository is type VacationRepository
  # of type depends.  FastAPI is going to create an instance of this for us.

  return_response = repo.create(vacation)

  if type(return_response) is not VacationOut:
    response.status_code = 400

  return return_response

# GET VACATIONS
@router.get("/vacations", response_model=Union[List[VacationOut], Error])
def get_all(
  repo: VacationRepository = Depends()
):
  return repo.get_all()

# UPDATE VACATION
@router.put("/vacations/{vacation_id}", response_model=Union[VacationOut, Error])
def update_vacation(
  vacation_id: int,
  vacation: VacationIn,
  repo: VacationRepository = Depends(),
) -> Union[Error, VacationOut]:
  return repo.update(vacation_id, vacation)

# DELETE VACATION
@router.delete("/vacations/{vacation_id}", response_model=bool)
def delete_vacation(
  vacation_id: int,
  repo: VacationRepository = Depends(),
) -> bool:
  return repo.delete(vacation_id)

# GET A VACATION
@router.get("/vacations/{vacation_id}", response_model=Union[VacationOut, Error])
def get_vacation(
  vacation_id: int,
  response: Response,
  repo: VacationRepository = Depends(),
) -> VacationOut:
  result = repo.get_one(vacation_id)

  if result == None:
    response.status_code = 400
  return result