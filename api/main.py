from fastapi import FastAPI
from routers import vacations


app = FastAPI()
# app is what is run by uvicorn in docker-compose
app.include_router(vacations.router)